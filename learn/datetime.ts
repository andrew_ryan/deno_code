import * as datetime from "https://gitcode.net/dnrops/deno_std/-/raw/main/datetime/mod.ts";

console.log(datetime.dayOfYear(new Date("2019-03-11T03:24:00")));
console.log(datetime.weekOfYear(new Date("2019-03-11T03:24:00")));

let diff = datetime.difference(new Date("2020/1/1"), new Date("2020/2/2"), {
  units: ["days", "months"],
});
console.log(diff);
