import { read, write_append } from "./lib.ts";

let run = async () => {
  clear_file("./rest.rs");
  let c = await read("./main.c");
  let c_arr = c.split("\n");
  for (const item of c_arr) {
    if (item.includes("main(")) {
      await write_append("./rest.rs", "fn main()");
    } else if (item.includes("{")) {
      await write_append("./rest.rs", "{");
    } else if (item.includes("int") && item.includes("=")) {
      let new_item: any = item.trim();
      let re = `let ${new_item.split("=")[0].split(" ")[1]}:i64=${
        new_item.split("=")[1]
      }`;
      await write_append(
        "./rest.rs",
        [re].join(),
      );
    } else if (item.includes("printf(")) {
      if (!item.includes("%")) {
        let connent = item;
        let re = connent.match(/\"\w{0,}\"/g);
        await write_append(
          "./rest.rs",
          ['println!("{:?}"', re].join(),
        );
        await write_append(
          "./rest.rs",
          [");"].join(),
        );
      } else {
        let re = item.split(",")[1].split(")")[0];
        await write_append(
          "./rest.rs",
          ['println!("{:?}"', re].join(),
        );
        await write_append(
          "./rest.rs",
          [");"].join(),
        );
      }
    } else if (item.includes("}")) {
      await write_append("./rest.rs", "}");
    }
  }
};

let clear_file = async (file_name: string) => {
  let data = "";
  Deno.writeFile(file_name, new TextEncoder().encode(data), { append: false });
};
await run();
