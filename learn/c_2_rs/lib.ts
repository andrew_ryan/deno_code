let read = async (file_name: string) => {
  let file = await Deno.readFile(file_name);
  let file_string = new TextDecoder().decode(file);
  return file_string;
};
let write_append = async (file_name: string, data: string) => {
  Deno.writeFile(file_name, new TextEncoder().encode(data), { append: true });
};

export { read, write_append };
