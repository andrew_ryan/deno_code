import { serve } from "https://gitcode.net/dnrops/deno_std/-/raw/main/http/server.ts";
import "https://deno.land/x/corejs@v3.22.1/index.js"; // <- at the top of your entry point

function handler(_req) {
  const data = {
    Hello: "World!",
  };
  const body = JSON.stringify(data, null, 2);
  return new Response(body, {
    headers: { "content-type": "application/json; charset=utf-8" },
  });
}

console.log("Listening on http://localhost:8000");
serve(handler);

Object.hasOwn({ foo: 42 }, "foo"); // => true

[1, 2, 3, 4, 5, 6, 7].at(-3); // => 5

[1, 2, 3, 4, 5].groupBy((it) => it % 2); // => { 1: [1, 3, 5], 0: [2, 4] }

Promise.any([
  Promise.resolve(1),
  Promise.reject(2),
  Promise.resolve(3),
]).then(console.log); // => 1

[1, 2, 3, 4, 5, 6, 7].values()
  .drop(1)
  .take(5)
  .filter((it) => it % 2)
  .map((it) => it ** 2)
  .toArray(); // => [9, 25]
