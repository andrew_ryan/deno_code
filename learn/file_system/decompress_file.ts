const Decompression = async (zip_file: any, file: string) => {
  const input = await Deno.open(zip_file);
  const output = await Deno.create(file);

  input.readable
    .pipeThrough(new DecompressionStream("gzip"))
    .pipeTo(output.writable);
};

Decompression("./file.txt.gz", "./file.txt");
