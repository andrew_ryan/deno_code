const rename = async (dir: string, containts: string) => {
  let dir_list = [];
  for await (const dirEntry of Deno.readDir(dir)) {
    if (dirEntry.name.includes(containts)) {
      dir_list.push(dirEntry.name);
    }
  }
  for (let i = 0; i < dir_list.length; i++) {
    console.log(dir_list[i]);
    Deno.renameSync(dir_list[i], `./${i}${containts}`);
  }
};
rename("./", ".mp4");
