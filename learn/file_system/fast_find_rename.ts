import {
  readableStreamFromReader,
  writableStreamFromWriter,
} from "https://deno.land/std@0.136.0/streams/conversion.ts";
const p = Deno.run({
  cmd: ["fast_find", ".7z"],
  stdout: "piped",
  stderr: "piped",
});

const { code } = await p.status();

// Reading the outputs closes their pipes
const rawOutput = await p.output();
const rawError = await p.stderrOutput();

//enumerate
function* enumerate(array: any[]) {
  for (let i = 0; i < array.length; i += 1) {
    yield [i, array[i]];
  }
}

if (code === 0) {
  // await Deno.stdout.write(rawOutput);
  let get_cmd_output: string = new TextDecoder().decode(rawOutput);
  let a = get_cmd_output.replaceAll("[", "").replaceAll("]", "").replaceAll(
    "\n",
    "",
  ).replaceAll(' "', "").replaceAll('"', "");
  const get_cmd_output_list = a.split(",").filter((r) => {
    if (r != "") return r;
  });
  let z_list: any[] = [];
  for (const item of get_cmd_output_list) {
    z_list.push(item);
  }
  console.log(z_list);
  for (let i = 0; i < z_list.length; i++) {
    Deno.renameSync(z_list[i], `./${i}.7z`);
  }
} else {
  const errorString = new TextDecoder().decode(rawError);
  console.log(errorString);
}

Deno.exit(code);
