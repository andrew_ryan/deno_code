let read = async (file_name: string) => {
  let file = await Deno.readFile(file_name);
  let file_string = new TextDecoder().decode(file);
  console.log(file_string);
};
read("./ok.txt");
