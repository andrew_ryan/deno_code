let write = (file_name: string, data: string) => {
  Deno.writeFile(file_name, new TextEncoder().encode(data));
};
write("ok.txt", "ok");
