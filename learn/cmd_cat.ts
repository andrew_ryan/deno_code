let arg: string[] = Deno.args;
if (arg.length == 1) {
  const p = Deno.run({
    cmd: ["cat", arg[0]],
    stdout: "piped",
    stderr: "piped",
  });
  const { code } = await p.status();
  const rawOutput = await p.output();
  const rawError = await p.stderrOutput();
  if (code === 0) {
    await Deno.stdout.write(rawOutput);
  } else {
    const errorString = new TextDecoder().decode(rawError);
    console.log(errorString);
  }
  Deno.exit(code);
}

console.log(Deno.env.get("HOME"));
///home/andry

//curren dir pwd
console.log(Deno.cwd());
///run/media/andry/DATA/code/deno_code/learn
