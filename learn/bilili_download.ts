import {
  readableStreamFromReader,
  writableStreamFromWriter,
} from "https://deno.land/std@0.136.0/streams/conversion.ts";

const download = async (url: string) => {
  const p = Deno.run({
    cmd: ["you-get", "-d", "--playlist", url],
    stdout: "piped",
    stderr: "piped",
  });

  const { success } = await p.status();

  // Reading the outputs closes their pipes
  const rawOutput = await p.output();
  const rawError = await p.stderrOutput();

  if (success === true) {
    await Deno.stdout.write(rawOutput);
    let get_cmd_output: string = new TextDecoder().decode(rawOutput);
    console.log(get_cmd_output);
  } else {
    const errorString = new TextDecoder().decode(rawError);
    console.log(errorString);
  }
  //   Deno.exit(code);
};

let list: string[] = [
  "https://www.bilibili.com/bangumi/play/ss28169",
  "https://www.bilibili.com/bangumi/play/ss28484",
  "https://www.bilibili.com/bangumi/play/ss24414",
  "https://www.bilibili.com/bangumi/play/ss27588",
  "https://www.bilibili.com/bangumi/play/ss25248",
];
for (const item of list) {
  await download(item);
}
